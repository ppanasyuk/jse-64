package ru.panasyuk.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.panasyuk.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public final class TaskRepository {

    private Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("Task 1"));
        add(new Task("Task 2"));
        add(new Task("Task 3"));
        add(new Task("Task 4"));
    }

    public void add(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public void create() {
        add(new Task("Task " + System.currentTimeMillis()));
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

    public void save(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

}
