package ru.panasyuk.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.panasyuk.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public final class ProjectRepository {

    private Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("Project 1"));
        add(new Project("Project 2"));
        add(new Project("Project 3"));
        add(new Project("Project 4"));
    }

    public void add(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void create() {
        add(new Project("Project " + System.currentTimeMillis()));
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

    public void save(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

}