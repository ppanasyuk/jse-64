package ru.t1.panasyuk.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO> extends IDtoRepository<M> {

    @Nullable
    M add(@NotNull String userId, @Nullable M model);

    void clear(@NotNull String userId);

    @Nullable
    List<M> findAll(@NotNull String userId);

    @Nullable
    List<M> findAll(@NotNull String userId, @Nullable Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String userId, @Nullable String id);

    M findOneByIndex(@NotNull String userId, @Nullable Integer index);

    int getSize(@NotNull String userId);

}