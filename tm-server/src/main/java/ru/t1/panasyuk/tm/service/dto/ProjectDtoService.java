package ru.t1.panasyuk.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.service.dto.IProjectDtoService;
import ru.t1.panasyuk.tm.comparator.NameComparator;
import ru.t1.panasyuk.tm.comparator.StatusComparator;
import ru.t1.panasyuk.tm.constant.FieldConst;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.ProjectNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.repository.dto.ProjectDtoRepository;

import java.util.Comparator;
import java.util.List;

@Service
public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDTO, ProjectDtoRepository>
        implements IProjectDtoService {

    @NotNull
    @Override
    @Transactional
    public ProjectDTO changeProjectStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO changeProjectStatusByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final ProjectDTO project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @Override
    public long getSize(@NotNull final String userId) {
        long result;
        @NotNull final ProjectDtoRepository repository = getRepository();
        result = repository.countByUserId(userId);
        return result;
    }

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        @NotNull final ProjectDtoRepository repository = getRepository();
        repository.deleteByUserId(userId);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(userId, project);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setUserId(userId);
        return add(userId, project);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @Nullable final String id) {
        boolean result;
        @NotNull final ProjectDtoRepository repository = getRepository();
        result = repository.findFirstByUserIdAndId(userId, id) != null;
        return result;
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @Nullable List<ProjectDTO> models;
        @NotNull final ProjectDtoRepository repository = getRepository();
        @NotNull String field = FieldConst.FIELD_CREATED;
        if (comparator == NameComparator.INSTANCE) field = FieldConst.FIELD_NAME;
        else if (comparator == StatusComparator.INSTANCE) field = FieldConst.FIELD_STATUS;
        @NotNull org.springframework.data.domain.Sort sort = org.springframework.data.domain.Sort.by(
                org.springframework.data.domain.Sort.Direction.DESC,
                field);
        models = repository.findByUserId(userId, sort);
        return models;
    }

    @Nullable
    @Override
    @SuppressWarnings("unchecked")
    public List<ProjectDTO> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        final Comparator<ProjectDTO> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final ProjectDTO model;
        @NotNull final ProjectDtoRepository repository = getRepository();
        @NotNull final Pageable pageable = PageRequest.of(index - 1, 1);
        model = repository
                .findByIndex(userId, pageable)
                .stream()
                .findFirst()
                .orElse(null);
        return model;
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null) return null;
        @Nullable final ProjectDTO model;
        @NotNull final ProjectDtoRepository repository = getRepository();
        model = repository.findFirstByUserIdAndId(userId, id);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final ProjectDTO removedModel;
        @NotNull final ProjectDtoRepository repository = getRepository();
        @NotNull final Pageable pageable = PageRequest.of(index - 1, 1);
        removedModel = repository
                .findByIndex(userId, pageable)
                .stream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
        repository.delete(removedModel);
        return removedModel;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO removedModel;
        @NotNull final ProjectDtoRepository repository = getRepository();
        removedModel = repository.findFirstByUserIdAndId(userId, id);
        if (removedModel == null) throw new EntityNotFoundException();
        repository.delete(removedModel);
        return removedModel;
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        @Nullable final List<ProjectDTO> models;
        @NotNull final ProjectDtoRepository repository = getRepository();
        @NotNull final org.springframework.data.domain.Sort sort = org.springframework.data.domain.Sort.by(org.springframework.data.domain.Sort.Direction.DESC, "created");
        models = repository.findByUserId(userId, sort);
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO updateById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO updateByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final ProjectDTO project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

}