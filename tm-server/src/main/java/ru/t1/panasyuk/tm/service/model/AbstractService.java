package ru.t1.panasyuk.tm.service.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.service.model.IService;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IdEmptyException;
import ru.t1.panasyuk.tm.model.AbstractModel;
import ru.t1.panasyuk.tm.repository.model.AbstractRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public abstract class AbstractService<M extends AbstractModel, R extends AbstractRepository<M>>
        implements IService<M> {

    @Getter
    @NotNull
    @Autowired
    private R repository;

    @NotNull
    @Override
    @Transactional
    public M add(@NotNull final M model) {
        @NotNull final R repository = getRepository();
        repository.save(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> add(@NotNull final Collection<M> models) {
        @NotNull final R repository = getRepository();
        repository.saveAll(models);
        return models;
    }

    @Override
    @Transactional
    public void clear() {
        @NotNull final R repository = getRepository();
        repository.deleteAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        boolean result;
        @NotNull final R repository = getRepository();
        result = repository.findById(id).isPresent();
        return result;
    }

    @Nullable
    @Override
    public List<M> findAll() {
        @Nullable final List<M> models;
        @NotNull final R repository = getRepository();
        models = repository.findAll();
        return models;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null) return null;
        @Nullable M model = null;
        @NotNull final R repository = getRepository();
        Optional<M> result = repository.findById(id);
        if (result.isPresent())
            model = result.get();
        return model;
    }

    @Override
    public long getSize() {
        long result;
        @NotNull final R repository = getRepository();
        result = repository.count();
        return result;
    }

    @Nullable
    @Override
    @Transactional
    public M remove(@NotNull final M model) {
        @Nullable final M removedModel;
        @NotNull final R repository = getRepository();
        removedModel = repository.findById(model.getId()).orElseThrow(EntityNotFoundException::new);
        repository.delete(removedModel);
        return removedModel;
    }

    @Nullable
    @Override
    @Transactional
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M removedModel;
        @NotNull final R repository = getRepository();
        removedModel = repository.findById(id).orElseThrow(EntityNotFoundException::new);
        repository.delete(removedModel);
        return removedModel;
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        @NotNull final R repository = getRepository();
        repository.save(model);
    }

}