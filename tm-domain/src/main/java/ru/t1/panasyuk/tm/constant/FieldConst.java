package ru.t1.panasyuk.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class FieldConst {

    @NotNull
    public static final String FIELD_CREATED = "created";

    @NotNull
    public static final String FIELD_EMAIL = "email";

    @NotNull
    public static final String FIELD_ID = "id";

    @NotNull
    public static final String FIELD_LOGIN = "login";

    @NotNull
    public static final String FIELD_NAME = "name";

    @NotNull
    public static final String FIELD_PROJECT = "project";

    @NotNull
    public static final String FIELD_PROJECT_ID = "projectId";

    @NotNull
    public static final String FIELD_SESSION = "session";

    @NotNull
    public static final String FIELD_STATUS = "status";

    @NotNull
    public static final String FIELD_TASK = "task";

    @NotNull
    public static final String FIELD_USER_ID = "userId";

    @NotNull
    public static final String FIELD_USER = "user";

    private FieldConst() {
    }

}