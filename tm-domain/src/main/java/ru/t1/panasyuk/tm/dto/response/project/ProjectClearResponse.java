package ru.t1.panasyuk.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectClearResponse extends AbstractProjectResponse {

    public ProjectClearResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}