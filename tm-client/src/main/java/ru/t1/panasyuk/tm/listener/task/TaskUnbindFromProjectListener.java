package ru.t1.panasyuk.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.t1.panasyuk.tm.dto.response.task.TaskUnbindFromProjectResponse;
import ru.t1.panasyuk.tm.event.ConsoleEvent;
import ru.t1.panasyuk.tm.util.TerminalUtil;

@Component
public final class TaskUnbindFromProjectListener extends AbstractTaskListener {

    @NotNull
    private static final String DESCRIPTION = "Unbind task from project.";

    @NotNull
    private static final String NAME = "task-unbind-from-project";

    @Override
    @EventListener(condition = "@taskUnbindFromProjectListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken(), projectId, taskId);
        @NotNull final TaskUnbindFromProjectResponse response = taskEndpoint.unbindTaskFromProject(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}